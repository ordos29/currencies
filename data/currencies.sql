create database currencies;
use currencies;
create table currencies
(
    id        int unsigned auto_increment primary key,
    num_code  smallint unsigned       not null,
    char_code varchar(5)              not null,
    valute_id varchar(10)             not null,
    name      varchar(255)            not null,
    constraint currencies_num_code_uindex unique (num_code)
);

create table rates
(
    id          int unsigned auto_increment primary key,
    currency_id int unsigned       not null,
    date        date               not null,
    req_date    date               not null,
    nominal     mediumint unsigned not null,
    value     decimal(11, 4) unsigned not null
);

create index currency_idx on rates (currency_id);

create index date_idx on rates (date desc);
