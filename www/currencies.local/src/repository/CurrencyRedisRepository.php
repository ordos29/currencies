<?php

namespace src\repository;

use Redis;
use src\filter\Filter;

class CurrencyRedisRepository
{
    private const CURRENCIES_LIST_TIMEOUT = 60 * 60 * 24;

    private Redis $redis;

    /**
     * CurrencyRedisRepository constructor.
     * @param Redis $redis
     */
    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    public function getRates(Filter $filter): array
    {
        $requestedDates = $filter->getInterval();
        $redisDates = $this->redis->keys('*');
        $existedDates = array_intersect($requestedDates, $redisDates);
        $redisRates = (array)$this->redis->mget($existedDates);
        $redisRatesWithDates = [];
        foreach ($redisRates as $redisRate) {
            $redisRate = json_decode($redisRate, true);
            if (!empty($redisRate)) {
                $redisRatesWithDates[$redisRate[0]['req_date']] = $redisRate;
            }
        }

        return $redisRatesWithDates;
    }

    public function getNonExistedDatesForFilter(Filter $filter): array
    {
        $requestedDates = $filter->getInterval();
        $redisDates = $this->redis->keys('*');

        return array_diff($requestedDates, $redisDates);
    }

    public function setRatesForDate(string $requestDate, array $rates): bool
    {
        return $this->redis->setex(
            $requestDate,
            self::CURRENCIES_LIST_TIMEOUT,
            json_encode($rates)
        );
    }

}
