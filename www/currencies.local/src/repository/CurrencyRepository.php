<?php

namespace src\repository;

use PDO;
use src\container\Container;
use src\filter\Filter;

class CurrencyRepository
{
    private const GET_CURRENCY_LIST_SQL = <<<SQL
SELECT r.date, r.req_date, c.valute_id, c.num_code, c.char_code, r.nominal, c.name, r.value
FROM rates r
LEFT JOIN currencies c ON c.id = r.currency_id
WHERE r.req_date IN (:dates)
ORDER BY r.req_date;
SQL;

    private const GET_NON_EXISTED_RATES = <<<SQL
SELECT DISTINCT (req_date) 
FROM rates
WHERE req_date >= :start_date AND req_date <= :end_date
ORDER BY req_date;
SQL;

    private const GET_EXISTED_CURRENCIES = 'SELECT id, num_code FROM currencies;';

    private const ADD_CURRENCY = <<<SQL
INSERT INTO currencies (num_code, char_code, valute_id, name) 
VALUES (:num_code, :char_code, :valute_id, :name);
SQL;

    private const ADD_RATE = <<<SQL
INSERT INTO rates (currency_id, date, req_date, nominal, value) 
VALUES (:currency_id, :date, :req_date, :nominal, :value);
SQL;

    private PDO $db;
    private CurrencyRedisRepository $redisRepository;

    public function __construct(PDO $db)
    {
        $this->db = $db;
        $this->redisRepository = new CurrencyRedisRepository(Container::get('redis'));
    }

    public function getCurrencyList(Filter $filter): array
    {
        $in = implode(', ', array_fill(0, count($filter->getInterval()), '?'));
        $query = str_replace(':dates', $in, self::GET_CURRENCY_LIST_SQL);
        $statement = $this->db->prepare($query);
        $statement->execute(array_values($filter->getInterval()));
        $sqlRates = $statement->fetchAll(PDO::FETCH_ASSOC);
        $sqlRatesWithDate = [];
        foreach ($sqlRates as $sqlRate) {
            $sqlRatesWithDate[$sqlRate['req_date']][] = $sqlRate;
        }
        foreach ($sqlRatesWithDate as $date => $sqlRateWithDate) {
            $this->redisRepository->setRatesForDate($date, $sqlRateWithDate);
        }

        return $sqlRatesWithDate;
    }

    public function getNonExistedDatesForFilter(Filter $filter): array
    {
        $statement = $this->db->prepare(self::GET_NON_EXISTED_RATES);
        $statement->execute([':start_date' => $filter->getFrom(), ':end_date' => $filter->getTo()]);
        $sqlRates = $statement->fetchAll(PDO::FETCH_COLUMN);

        return array_diff($filter->getInterval(), $sqlRates);
    }

    public function getExistedCurrencies(): array
    {
        $existedCurrencies = [];
        $existedCurrenciesFromBase = $this->db->query(self::GET_EXISTED_CURRENCIES)->fetchAll(PDO::FETCH_ASSOC);
        foreach ($existedCurrenciesFromBase as $currency) {
            $existedCurrencies[$currency['num_code']] = $currency['id'];
        }

        return $existedCurrencies;
    }

    public function addCurrencies(array $currencies)
    {
        if (empty($currencies)) {
            return;
        }
        $statement = $this->db->prepare(self::ADD_CURRENCY);

        foreach ($currencies as $currency) {
            $statement->bindValue(':num_code', $currency['num_code']);
            $statement->bindValue(':char_code', $currency['char_code']);
            $statement->bindValue(':valute_id', $currency['valute_id']);
            $statement->bindValue(':name', $currency['name']);
            $statement->execute();
        }
    }

    public function addRatesForDate(string $requestDate, array $rates)
    {
        if (empty($rates)) {
            return;
        }
        $statement = $this->db->prepare(self::ADD_RATE);

        foreach ($rates as &$rate) {
            $statement->bindValue(':currency_id', $rate['currency_id']);
            $statement->bindValue(':date', $rate['date']);
            $statement->bindValue(':req_date', $requestDate);
            $statement->bindValue(':nominal', $rate['nominal']);
            $statement->bindValue(':value', $rate['value']);
            $statement->execute();
            unset($rate['currency_id']);
        }

        $this->redisRepository->setRatesForDate($requestDate, $rates);
    }

}
