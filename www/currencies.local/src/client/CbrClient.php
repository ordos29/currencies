<?php

namespace src\client;

use DateTime;
use src\container\Container;
use src\filter\Filter;
use src\repository\CurrencyRedisRepository;
use src\repository\CurrencyRepository;

class CbrClient
{
    private CurrencyRepository $currencyRepository;
    private CurrencyRedisRepository $redisRepository;

    public function __construct()
    {
        $this->currencyRepository = new CurrencyRepository(Container::get('db'));
        $this->redisRepository = new CurrencyRedisRepository(Container::get('redis'));
    }

    public function getCurrencyList(Filter $filter): array
    {
        $ratesFromService = [];
        foreach ($filter->getInterval() as $date) {
            $ratesFromService[$date] = $this->getCurrencyListForDate((DateTime::createFromFormat('Y-m-d', $date)));
        }

        return $ratesFromService;
    }

    private function getCurrencyListForDate(DateTime $requestDate): array
    {
        $newRates = [];
        $newCurrencies = [];
        $cbrRates = simplexml_load_file(getenv('RATES_URL') . "?date_req={$requestDate->format('d/m/Y')}");
        $attributes = $cbrRates->attributes();
        $valuteDate = $attributes->Date->__toString();
        $valuteDate = (DateTime::createFromFormat('d.m.Y', $valuteDate))->format('Y-m-d');
        $existedCurrencies = $this->currencyRepository->getExistedCurrencies();

        foreach ($cbrRates->Valute as $valute) {
            if (!array_key_exists($valute->NumCode->__toString(), $existedCurrencies)) {
                $newCurrencies[] = [
                    'num_code' => $valute->NumCode->__toString(),
                    'char_code' => $valute->CharCode->__toString(),
                    'valute_id' => $valute->attributes()->ID->__toString(),
                    'name' => $valute->Name->__toString(),
                ];
            }
        }
        $this->currencyRepository->addCurrencies($newCurrencies);
        $existedCurrencies = $this->currencyRepository->getExistedCurrencies();

        foreach ($cbrRates->Valute as $valute) {
            $newRates[] = [
                'valute_id' => $valute->attributes()->ID->__toString(),
                'num_code' => $valute->NumCode->__toString(),
                'char_code' => $valute->CharCode->__toString(),
                'currency_id' => $existedCurrencies[$valute->NumCode->__toString()],
                'date' => $valuteDate,
                'req_date' => $requestDate->format('Y-m-d'),
                'nominal' => $valute->Nominal->__toString(),
                'name' => $valute->Name->__toString(),
                'value' => str_replace(',', '.', $valute->Value->__toString()),
            ];
        }
        $this->currencyRepository->addRatesForDate($requestDate->format('Y-m-d'), $newRates);
        foreach ($newRates as &$newRate) {
            unset($newRate['currency_id']);
        }

        return $newRates;
    }

}
