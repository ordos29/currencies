<?php

namespace src\config;

class Config
{
    public static function getRoutes(): array
    {
        return [
            'GET' => [
                '/api/currencies/list' => '\src\action\ListAction'
            ]
        ];
    }

}
