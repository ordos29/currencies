<?php

namespace src\container;

use PDO;
use Redis;
use src\exception\ServiceNotFoundException;

class Container
{
    /**
     * @param string $service
     * @return object
     * @throws ServiceNotFoundException
     */
    public static function get(string $service): object
    {
        switch ($service) {
            case 'redis':
                $redis = new Redis();
                $redis->connect(getenv('REDIS_HOST'));
                $redis->auth(getenv('REDIS_PASSWORD'));

                return $redis;
            case 'db':
                $dbHost = getenv('DB_HOST');
                $dbName = getenv('DB_NAME');
                $dbUser = getenv('DB_USER');
                $dbPassword = getenv('DB_PASSWORD');

                return new PDO("mysql:host={$dbHost};dbname={$dbName}", $dbUser, $dbPassword);
            default:
                throw new ServiceNotFoundException("Service '{$service}' not found");
        }
    }

}
