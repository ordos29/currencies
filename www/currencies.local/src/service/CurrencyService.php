<?php

namespace src\service;

use src\client\CbrClient;
use src\container\Container;
use src\filter\Filter;
use src\repository\CurrencyRedisRepository;
use src\repository\CurrencyRepository;

class CurrencyService
{
    private CurrencyRedisRepository $redisRepository;
    private CurrencyRepository $currencyRepository;
    private CbrClient $cbrClient;
    private Filter $filter;

    public function __construct(Filter $filter)
    {
        $this->redisRepository = new CurrencyRedisRepository(Container::get('redis'));
        $this->currencyRepository = new CurrencyRepository(Container::get('db'));
        $this->cbrClient = new CbrClient();
        $this->filter = $filter;
    }

    public function getCurrencyList(): array
    {
        $redisRates = $this->redisRepository->getRates($this->filter);

        $sqlFilter = clone $this->filter;
        $notExistedInRedis = $this->redisRepository->getNonExistedDatesForFilter($sqlFilter);
        if (empty($notExistedInRedis)) {
            return $redisRates;
        }

        $sqlFilter->setInterval($notExistedInRedis);
        $sqlRates = $this->currencyRepository->getCurrencyList($sqlFilter);
        $serviceFilter = clone $sqlFilter;
        $notExistedInSql = $this->currencyRepository->getNonExistedDatesForFilter($serviceFilter);
        if (empty($notExistedInSql)) {
            return array_merge($redisRates, $sqlRates);
        }

        $serviceFilter->setInterval($notExistedInSql);
        $serviceRates = $this->cbrClient->getCurrencyList($serviceFilter);

        return array_merge($redisRates, $sqlRates, $serviceRates);
    }

}
