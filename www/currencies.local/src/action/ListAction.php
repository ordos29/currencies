<?php

namespace src\action;

use src\filter\Filter;
use src\service\CurrencyService;

class ListAction implements Action
{
    public function execute()
    {
        $filter = new Filter($_GET['filter'] ?? []);
        $currencyService = new CurrencyService($filter);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($currencyService->getCurrencyList());
    }

}
