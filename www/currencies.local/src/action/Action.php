<?php

namespace src\action;

interface Action
{
    public function execute();
}