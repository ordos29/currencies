<?php

namespace src\filter;

use DateInterval;
use DatePeriod;
use DateTime;

class Filter
{
    private string $from;
    private string $to;
    private array $interval;

    public function __construct(array $request)
    {
        $this->from = $request['from'] ?? (new DateTime())->format('Y-m-d');
        $this->to = $request['to'] ?? (new DateTime())->format('Y-m-d');
        if ($this->from !== $this->to) {
            $period = new DatePeriod(
                new DateTime($this->from),
                new DateInterval('P1D'),
                (new DateTime($this->to))->add(new DateInterval('P1D'))
            );
            foreach ($period as $date) {
                $this->interval[] = $date->format('Y-m-d');
            }
        } else {
            $this->interval = [$this->from];
        }
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return string[]
     */
    public function getInterval(): array
    {
        return $this->interval;
    }

    /**
     * @param array $interval
     */
    public function setInterval(array $interval): void
    {
        $this->interval = $interval;
    }

}
