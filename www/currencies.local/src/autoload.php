<?php
spl_autoload_register(
    function ($className) {
        $filename = str_replace('\\', '/', $className);
        include $filename . '.php';
    }
);