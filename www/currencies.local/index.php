<?php
use src\action\Action;
use src\config\Config;

require_once 'src/autoload.php';

$method = $_SERVER['REQUEST_METHOD'];
$url = parse_url($_SERVER['REQUEST_URI']);
$path = $url['path'];
$routes = Config::getRoutes();
if (isset($routes[$method][$path])) {
    $actionName = $routes[$method][$path];
    /** @var Action $action */
    $action = new $actionName;
    $action->execute();
} else {
    header("HTTP/1.0 404 Not Found");
}
